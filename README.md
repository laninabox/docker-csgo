## Tags

* `latest`

## Base image

* `laninabox/steamcmd`

## Environment Variables

* `$LAN` - set to 0 or 1 for a LAN server, default  is 1
* `$HOSTNAME` - name of the gameserver, default is "CS:GO Server"
* `GAME_TYPE` - default is 0
* `GAME_MODE` - default is 1
* `MAP` - default is 'cs_office'
* `OTHER_ARGS` - default is ''
* `SOURCETV_ADDR` - default is ''

## Included files/directories

* `files/start.sh` - startup script
* `filescfg/csgo-server.cfg` - the default config file to load
* `files/cfg/esl/` - ESL config files for competitions

## Examples

Start the container with the ESL config files:

    docker run --rm -ti laninabox/csgo -e"OTHER_ARGS='-exec esl5on5.cfg'"

You can run this container with your own config files or motd.txt like so:

    docker run --rm -ti laninabox/csgo \
    -v "/home/username/own-server.cfg":"/steam/csgo/csgo/cfg/hostname.cfg" \
    -v "/home/username/own-motd.txt":"/steam/csgo/csgo/motd.txt" 